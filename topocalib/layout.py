import numpy as np
from .model import get_rotation, get_angles
import matplotlib.pyplot as plt
from numpy.linalg import inv
from mpl_toolkits.mplot3d import Axes3D



def set_axes_radius(ax, origin, radius):
    ax.set_xlim3d([origin[0] - radius, origin[0] + radius])
    ax.set_ylim3d([origin[1] - radius, origin[1] + radius])
    ax.set_zlim3d([origin[2] - radius, origin[2] + radius])


def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])

    origin = np.mean(limits, axis=1)
    radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
    set_axes_radius(ax, origin, radius)


def generate_layout(step=0.2, slope=np.pi/4, rotationstep=0.1, distance=5, plainsize=(6, 6), nplains=2, plaindist=2, planetransl=[-3, -3, 0], inv=-1):
    # Generate rotation and translation of cameras
    Ts = np.zeros(shape=(0, 3))
    Rs = np.zeros(shape=(0, 3))

    vec = np.array([0, 0, distance]) 

    for i in np.arange(0, 2 * np.pi, step):
        T = (vec * get_rotation([0, slope, i]))
        Ts = np.concatenate((Ts, T), axis=0)
        
        z = T.A1 / np.linalg.norm(T)
        x = np.array([
            np.sin(i),
            -np.cos(i),
            0
        ])
        y = np.cross(x, z)
        rotation_matrix = np.matrix([
            [x[0], y[0], z[0]],
            [x[1], y[1], z[1]],
            [x[2], y[2], z[2]],
        ])
        angles = get_angles(rotation_matrix)
        
        Rs = np.concatenate((Rs, np.matrix(angles)), axis=0)

    # Generate points
    chess = np.zeros(shape=(plainsize[0] * plainsize[1], 3))
    for i in range(plainsize[0] * plainsize[1]):
        chess[i] = np.array([ (i % plainsize[0]) * 0.5, int(i / plainsize[0]) * 0.5, 0])

    multi_chess = np.zeros(shape=(0, 3))
    for i in range(nplains):
        single_chess = np.copy(chess) 
        single_chess += np.array([0, 0, plaindist * i])
        single_chess += np.array(planetransl)
        multi_chess = np.concatenate((multi_chess, single_chess), axis=0)
        
    X_ws = []
    for i in range(len(Ts)):
        X_ws.append(multi_chess)

    return Ts, Rs, X_ws
        
        
def show_layout(Ts, Rs, X_ws, figsize=(7, 7), ax=None):
    if ax is None:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, projection='3d')
        
    # ax.set_aspect('equal')
    ax.scatter(X_ws[0][:, 0], X_ws[0][:, 1], X_ws[0][:, 2], label='Points')
    
    image_i = 0
    colors = ['red', 'blue', 'green']
    for T, R in zip(Ts, Rs):
        Xc1 = T.A1
        R = R.A1
        
        for axis_i in range(0, 3):
            axis = np.array([0, 0, 0])
            axis[axis_i] = 1

            Xc2 = axis * get_rotation(R).T + Xc1
            Xc2 = Xc2.A1

            ax.plot(
                [Xc1[0], Xc2[0]], 
                [Xc1[1], Xc2[1]], 
                [Xc1[2], Xc2[2]],
                linewidth=2,
                color=colors[axis_i]
            )
            ax.text(Xc2[0], Xc2[1], Xc2[2], 'XYZ'[axis_i])
        image_i += 1

    ax.set_xlabel('X coordinate')
    ax.set_ylabel('Y coordinate')
    ax.set_zlabel('Z coordinate')
    set_axes_equal(ax)
    
    return ax

