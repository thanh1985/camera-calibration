import numpy as np
import cv2
from numpy.linalg import multi_dot


def get_rotation(R):
        coss = np.cos(R)
        sins = np.sin(R)

        A = np.matrix([
                [1, 0, 0],
                [0, coss[0], -sins[0]],
                [0, sins[0], coss[0]
        ]]) 
        B = np.matrix([
                [coss[1], 0, sins[1]],
                [0, 1, 0],
                [-sins[1], 0, coss[1]]
        ])
        C = np.matrix([
                [coss[2], -sins[2], 0],
                [sins[2], coss[2], 0],
                [0, 0, 1]
        ])

        return A*B*C # multi_dot([A, B, C])
    
    
def get_angles_old(R):
    """
    Reference: https://stackoverflow.com/questions/15022630/how-to-calculate-the-angle-from-rotation-matrix
    """
    theta_x = np.arctan2(R[2,1], R[2, 2])
    theta_y = np.arctan2(-R[2,0], np.sqrt(R[2, 1]**2 + R[2, 2]**2))
    theta_z = np.arctan2(R[1, 0], R[0, 0])
    
    # theta_y = - np.arcsin(R[0, 2])
    # theta_x = np.arctan2(R[1,2] / np.cos(theta_y), R[2, 2] / np.cos(theta_y));
    
    return np.array([ theta_x, theta_y, theta_z ])
        
    
def get_angles(R):
    """
    Reference: https://www.geometrictools.com/Documentation/EulerAngles.pdf
    """
    if R[0,2] < 1:
        if R[0,2] > -1:
            thetaY = np.arcsin(R[0,2])
            thetaX = np.arctan2(- R[1,2], R[2,2])
            thetaZ = np.arctan2(- R[0,1], R[0,0])
        else:
            thetaY = -np.pi / 2
            thetaX = -np.arctan2(R[1,0], R[1,1])
            thetaZ = 0
    else:
        thetaY = np.pi / 2
        thetaX = np.arctan2(R[1,0], R[1,1])
        thetaZ = 0
        
    return np.array([ thetaX, thetaY, thetaZ ])